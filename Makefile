DOTFILES_DIR := $(shell echo $(HOME)/Projects/dotfiles)
SHELL := /bin/bash
UNAME_M := $(shell uname -m)
UNAME_S := $(shell uname -s)
USER := $(shell whoami)
STOW := stow

ifeq ($(UNAME_S), Linux)
	BASE := linux
endif

# Directories
BASH_DIR := bash/dots
VIM_DIR := vim
BASH_FILES := aliases env func git

# Default target
.PHONY: all install stow bash vim git tmux linux help usage ensure-stow vim-plugins

all: install

#install: bash vim
install: ensure-stow stow vim-plugins

ensure-stow:
	@if ! command -v $(STOW) &> /dev/null; then \
	    echo "Stow not found. Installing..."; \
	    if [ -x "$$(command -v apt)" ]; then \
	        sudo apt-get -y update && sudo apt-get install -y stow; \
	    elif [ -x "$$(command -v yum)" ]; then \
	        sudo yum install -y stow; \
	    elif [ -x "$$(command -v dnf)" ]; then \
	        sudo dnf install -y stow; \
	    elif [ -x "$$(command -v pacman)" ]; then \
	        sudo pacman -S --noconfirm stow; \
	    else \
	        echo "Package manager not supported. Please install Stow manually."; \
	        exit 1; \
	    fi \
	else \
	    echo "Stow is already installed."; \
	fi

stow:
	@echo "Stowing dotfiles..."
	[ -f ~/.bash_profile ] && [ ! -L ~/.bash_profile ] && mv ~/.bash_profile ~/.bash_profile.bak
	[ -f ~/.bashrc ] && [ ! -L ~/.bashrc ] && mv ~/.bashrc ~/.bashrc.bak
	$(STOW) -d $(DOTFILES_DIR) -t $(HOME) bash
	$(STOW) -d $(DOTFILES_DIR) -t $(HOME) git
	$(STOW) -d $(DOTFILES_DIR) -t $(HOME) tmux
	$(STOW) -d $(DOTFILES_DIR) -t $(HOME) vim

bash:
	@echo "Installing bash dotfiles..."
	$(STOW) -d $(DOTFILES_DIR) -t $(HOME) bash
	#@for file in $(BASH_FILES); do \
	#    cp $(BASH_DIR)/$$file $(HOME)/.$$file; \
	#    echo "Installed $$file"; \
	#done

vim:
	@echo "Installing vim dotfiles..."
	$(STOW) -d $(DOTFILES_DIR) -t $(HOME) vim
#	@cp $(VIM_DIR)/.vimrc $(HOME)/.vimrc
#	@vim +PlugInstall +qall

git:
	@echo "Stowing git dotfiles..."
	$(STOW) -d $(DOTFILES_DIR) -t $(HOME) git

tmux:
	@echo "Stowing tmux dotfiles..."
	$(STOW) -d $(DOTFILES_DIR) -t $(HOME) tmux

# get Vim to install its plugins
vim-plugins:
	@echo "Installing Vim plugins"
	vim +PlugInstall +qall

#linux: bash vim
linux: ensure-stow stow
	@echo "Installing linux-specific packages..."
	@sudo apt install -y build-essential

#test-bash: bash
#	@echo "This is a test run. The above commands show what would be executed."
#
#test-vim: vim
#	@echo "This is a test run. The above commands show what would be executed."

help: usage

usage:
	printf "\\n\
	\\033[1mDOTFILES\\033[0m\\n\
	\\n\
	Custom settings and configurations for Unix-like environments.\\n\
	See README.md for detailed usage information.\\n\
	\\n\
	\\033[1mUSAGE:\\033[0m make [target]\\n\
	\\n\
	  make         Install all configurations and applications.\\n\
	  make stow    Stow all configurations.\\n\
	  make bash    Stow bash configurations.\\n\
	  make git     Stow git configurations.\\n\
	  make tmux    Stow tmux configurations.\\n\
	  make vim     Stow vim configurations.\\n\
	  make linux   Install all configurations and linux-specific packages.\\n\
	  make test-stow Test stow all configurations.\\n\
	  make test-bash Test stow bash configurations.\\n\
	  make test-git Test stow git configurations.\\n\
	  make test-tmux Test stow tmux configurations.\\n\
	  make test-vim Test stow vim configurations.\\n\
	\\n\
	"

test-stow:
	@echo "Testing stow of dotfiles..."
	@echo "[ -f ~/.bash_profile ] && [ ! -L ~/.bash_profile ] && mv ~/.bash_profile ~/.bash_profile.bak"
	@echo "[ -f ~/.bashrc ] && [ ! -L ~/.bashrc ] && mv ~/.bashrc ~/.bashrc.bak"
	@echo "$(STOW) -d $(DOTFILES_DIR) -t $(HOME) bash"
	@echo "$(STOW) -d $(DOTFILES_DIR) -t $(HOME) git"
	@echo "$(STOW) -d $(DOTFILES_DIR) -t $(HOME) tmux"
	@echo "$(STOW) -d $(DOTFILES_DIR) -t $(HOME) vim"

test-bash:
	@echo "Testing stow of bash dotfiles..."
	@echo "$(STOW) -d $(DOTFILES_DIR) -t $(HOME) bash"

test-git:
	@echo "Testing stow of git dotfiles..."
	@echo "$(STOW) -d $(DOTFILES_DIR) -t $(HOME) git"

test-tmux:
	@echo "Testing stow of tmux dotfiles..."
	@echo "$(STOW) -d $(DOTFILES_DIR) -t $(HOME) tmux"

test-vim:
	@echo "Testing stow of vim dotfiles..."
	@echo "$(STOW) -d $(DOTFILES_DIR) -t $(HOME) vim"
