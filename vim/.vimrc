"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Edson's Vimrc
"   * Created based on https://dougblack.io/words/a-good-vimrc.html
"   * Created based on @ashu's vimrc
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" install plugins
" Plugins are managed by Vim-plug: https://github.com/junegunn/vim-plug
call plug#begin('~/.vim/bundle')
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tomtom/tlib_vim'
Plug 'MarcWeber/vim-addon-mw-utils'
Plug 'garbas/vim-snipmate'
Plug 'honza/vim-snippets'
Plug 'vim-ruby/vim-ruby'
Plug 'tomasr/molokai'
Plug 'jiangmiao/auto-pairs'
Plug 'ervandew/supertab'
Plug 'tpope/vim-fugitive'
Plug 'powerline/powerline'
Plug 'scrooloose/nerdtree'
Plug 'davidhalter/jedi-vim'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'ntpeters/vim-better-whitespace'
Plug 'tpope/vim-surround'
Plug 'jremmen/vim-ripgrep'
"Plug 'puremourning/vimspector' - for vim compiled with python3
Plug 'chrismccord/bclose.vim'
Plug 'sheerun/vim-polyglot'
Plug 'morhetz/gruvbox'
call plug#end()
" Plugin settings
let g:snipMate = { 'snippet_version' : 1 }
" Colors
syntax enable           " enable syntax processing
"   * vim-airline-theme
"let g:gruvbox_(option) = '(value)'
" let g:airline_theme='badwolf'
"let g:airline#extensions#tabline
colorscheme gruvbox
" UI Config
imap jj <ESC>           " leave insert mode with jj
map <C-n> :NERDTreeToggle<CR> " map nerdtree to ctrl+n
set number              "  show line numbers
set rnu                 " set relative numbering. creates hybrid
set showcmd             " show command in ottom bar
filetype indent on      " load filetype-specific indent files
set wildmenu            " visual autocomplete for command menu
set showmatch           " highlight matching [{()}]
set hidden              " hide buffers instead of closing them
set ruler
set laststatus=2        " always show a status line
set statusline=%F       " format statusline by filename
set confirm
set nostartofline       " Prevent the cursor from changing the current column when jumping to other lines within the window
set cursorline          " highlight current line
set cursorcolumn        " set a cursor column. Useful for indentation
set noswapfile
set nocompatible        " be iMproves - vim does have to be compatible with vi.
set fileformat=unix
let g:strip_whitespace_on_save=1  " Removes trailing whitespace at save
let g:rg_command = 'rg --vimgrep -S' " Enable vim-ripgrep smart mode
"set virtualedit=block     " position the cursor where there is no char. set for visual block mode"
" Window splitting
set splitbelow
set splitright
"split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
" Help search
nmap <silent> <RIGHT>            :cnext<CR>
nmap <silent> <RIGHT> <RIGHT>    :cnfile<CR><C-G>
nmap <silent> <LEFT> <LEFT>      :cprev<CR>
nmap <silent> <LEFT>             :cpfile<CR>
" Buffer switching
map <leader>p :bp<CR> " ,p previous buffer
map <leader>n :bn<CR> " ,n next buffer
map <leader>d :bd<CR> " ,d delete buffer
" Tab switching
nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>
" highlight cursorline
highlight Cursor ctermfg=white ctermbg=green cterm=bold guifg=white guibg=green gui=bold
highlight Cursorcolumn ctermfg=white ctermbg=18 cterm=bold guifg=white guibg=#000087 gui=bold
" GUI Interaction
set ttymouse=xterm2
set mouse=n
set backspace=indent,eol,start
" Searching
set incsearch           " search as characters are entered
set hlsearch            " highlight matches
set ignorecase          " ignore case when searching
set smartcase           " use case if any caps are used

" History
set history=10000        " Lengthen history
set undolevels=10000     " Lengthen undo history

" Git commit messages
au FileType gitcommit set tw=72


"Meraki Ruby White Space Rules
set expandtab
set tabstop=2 shiftwidth=2 softtabstop=2
set textwidth=80        " line length limit
set colorcolumn=80

"Python White Space Rules
"set tabstop=4
"set softtabstop=4
"set expandtab
"set shiftwidth=4
"set textwidth=79        " PEP8 line length limit
"set shiftround

"Makefile exception
autocmd FileType make setlocal noexpandtab tabstop=4 shiftwidth=4 softtabstop=4

"Automatic indentation
set autoindent
"Insert(paste) mode toggle for exception from autoindent
set pastetoggle=<F2>

"capfile has ruby syntax
au BufNewFile,BufRead capfile set filetype=ruby

"Highlight useless whitespace at the end of line
highlight WhiteSpaceEOL ctermbg=red guibg=red
match WhiteSpaceEOL /\s\+\%#\@!$/
filetype indent plugin on
syntax on
