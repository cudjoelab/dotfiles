---

# Dotfiles Management

This repository contains my personal dotfiles managed using GNU Stow. Dotfiles are configuration files and directories that customize the behavior of applications and shells in Unix-like operating systems. This setup allows me to maintain a consistent environment across different machines.

## Getting Started

To use these dotfiles on a new machine, follow these steps:

### Installation

Clone this repository to your home directory (or any preferred location):

```bash
git clone https://gitlab.com/cudjoelab/dotfiles.git ~/Projects/dotfiles
cd ~/Projects/dotfiles
```

Run GNU Stow to symlink the desired dotfiles to your home directory:

```bash
make install
```

This will symlink the dotfiles (`bash`, `git`, `tmux`, `vim`) from this repository to your home directory, ensuring that your configurations are applied.

### Testing

You can perform a test run to see what changes would be made without actually modifying any files:

```bash
make test-stow
```

This command will simulate the stowing process and show you the actions that would be taken.

### Individual Configurations

If you prefer to install only specific configurations, you can use the following commands:

- Stow Bash configurations:
  ```bash
  make bash
  ```

- Stow Git configurations:
  ```bash
  make git
  ```

- Stow Tmux configurations:
  ```bash
  make tmux
  ```

- Stow Vim configurations:
  ```bash
  make vim
  ```

### Linux Specific Configurations

If you're on a Linux system and want to install additional packages along with stowing the configurations, you can use:

```bash
make linux
```

This will install linux-specific packages (`build-essential`) using `apt` package manager.

## Contributing

Feel free to fork this repository, make changes, and submit pull requests. Suggestions and improvements are welcome!

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Acknowledgments

- GNU Stow - for providing a simple yet powerful tool for managing dotfiles.
- Inspiration from various dotfiles repositories and community setups.
